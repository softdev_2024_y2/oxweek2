/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxv2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class OXV2 {

    private static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char turn = 'O';
    private static int row;
    private static Scanner in = new Scanner(System.in);
    private static int col;
    private static int count = 0;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (checkError()) {
                continue;
            }
            if (isFinish()) {
                showTable();
                showResult();
                break;
            }
            switchTurn();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX Game ");
    }

    private static void showTable() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println(" ");
        }
    }

    private static void showTurn() {
        System.out.println("Turn " + turn);
    }

    private static void inputRowCol() {
        System.out.print("Please input row, col : ");
        row = in.nextInt() - 1;
        col = in.nextInt() - 1;
        //System.out.println("" + row + " " + col); Log variables
    }

    private static boolean checkError() {
        if (outOfTable()) {
            System.out.println("This position is out of the table!!!");
            return true;
        } else if (alreadyTaken()) {
            System.out.println("Someone already made a move!!!");
            return true;
        } else {
            table[row][col] = turn;
            count++;
            return false;
        }
    }

    private static boolean outOfTable() {
        if (row < 0 || col < 0 || row >= 3 || col >= 3) {
            return true;
        }
        return false;
    }

    private static boolean alreadyTaken() {
        if (table[row][col] != '-') {
            return true;
        }
        return false;
    }

    private static void switchTurn() {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }

    private static boolean isFinish() {
        if (checkWin()) {
            return true;
        }
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    private static boolean checkWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        if (count == 9 && checkWin()==false) {
            return true;
        }
        return false;      
    }

    private static boolean checkRow() {
        if (table[row][0] == turn && table[row][1] == turn && table[row][2] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkCol() {
        if (table[0][col] == turn && table[1][col] == turn && table[2][col] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        }
        if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        if (table[0][0] != '-' && table[0][0] == table[1][1] && table[1][1] == table[2][2]) {
            return true;
        }
        if (table[0][2] != '-' && table[0][2] == table[1][1] && table[1][1] == table[2][0]) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        return false;
    }

    private static void showResult() {
        if (checkWin()) {
            System.out.println(turn + " Win !!!");
        }
        if (checkDraw()) {
            System.out.println("Draw!!!");
        }

    }
}
